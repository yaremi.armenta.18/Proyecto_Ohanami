class ProblemaNombreJugadorVacio extends Error {}

class ProblemaNumeroJugadoresMenorQueMinimo extends Error{}

class ProblemaJugadoresMayorQueMaximo extends Error{}

class ProblemaJugadoresRepetidos extends Error{}

class ProblemasAzulesNegativas extends Error{}

class ProblemasDemasiadasCartas extends Error{}

class ProblemaJugadoresNoConcuerdan extends Error{}

class ProblemaVerdesNegativas extends Error{}

class ProblemaOrdenIncorrecto extends Error{}

class ProblemaAzulesMenores extends Error{}

class ProblemasDemasiadasAzules extends Error{}

class ProblemaNegrasNegativas extends Error {}

class ProblemaRosasNegativas extends Error{}

class ProblemasDemasiadasVerdes extends Error{}

class ProblemaDisminucionAzules extends Error {}

class ProblemaDisminucionVerdes extends Error {}

class ProblemaDemasiadasRosas extends Error {}

class ProblemaDemasiadasNegras extends Error{}