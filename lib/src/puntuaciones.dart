import 'dart:convert';
import '../paquete.dart';
import 'jugador.dart';

const int ninguna = 0;
const int maximoCartasPR1 = 10;
const numeroMaximoCartasR2 = 20;

class pRonda1 {
  final Jugador jugador;
  final int cuantasAzules;

  pRonda1({required this.jugador, required this.cuantasAzules}) {
    if (cuantasAzules < ninguna) throw ProblemasAzulesNegativas();
    if (cuantasAzules > maximoCartasPR1) throw ProblemasDemasiadasCartas();
  }

  Map<String, dynamic> toMap() {
    return {
      'jugador': jugador.toMap(),
      'cuantasAzules': cuantasAzules,
    };
  }

  factory pRonda1.fromMap(Map<String, dynamic> map) {
    return pRonda1(
      jugador: Jugador.fromMap(map['jugador']),
      cuantasAzules: map['cuantasAzules'],
    );
  }

  String toJson() => json.encode(toMap());

  factory pRonda1.fromJson(String source) =>
      pRonda1.fromMap(json.decode(source));
}

class pRonda2 {
  final Jugador jugador;
  final int cuantasAzules;
  final int cuantasVerdes;

  pRonda2(
      {required this.jugador,
      required this.cuantasAzules,
      required this.cuantasVerdes}) {
    if (cuantasAzules < ninguna) throw ProblemasAzulesNegativas();
    if (cuantasVerdes < ninguna) throw ProblemaVerdesNegativas();
  }

  Map<String, dynamic> toMap() {
    return {
      'jugador': jugador.toMap(),
      'cuantasAzules': cuantasAzules,
      'cuantasVerdes': cuantasVerdes,
    };
  }

  factory pRonda2.fromMap(Map<String, dynamic> map) {
    return pRonda2(
      jugador: Jugador.fromMap(map['jugador']),
      cuantasAzules: map['cuantasAzules'],
      cuantasVerdes: map['cuantasVerdes'],
    );
  }

  String toJson() => json.encode(toMap());

  factory pRonda2.fromJson(String source) =>
      pRonda2.fromMap(json.decode(source));
}

class pRonda3 {
  final Jugador jugador;
  final int cuantasAzules;
  final int cuantasVerdes;
  final int cuantasNegras;
  final int cuantasRosas;
  pRonda3(
      {required this.jugador,
      required this.cuantasAzules,
      required this.cuantasVerdes,
      required this.cuantasNegras,
      required this.cuantasRosas}) {
    if (cuantasAzules < ninguna) throw ProblemasAzulesNegativas();
    if (cuantasVerdes < ninguna) throw ProblemaVerdesNegativas();
    if (cuantasNegras < ninguna) throw ProblemaNegrasNegativas();
    if (cuantasRosas < ninguna) throw ProblemaRosasNegativas();
  }

  Map<String, dynamic> toMap() {
    return {
      'jugador': jugador.toMap(),
      'cuantasAzules': cuantasAzules,
      'cuantasVerdes': cuantasVerdes,
      'cuantasNegras': cuantasNegras,
      'cuantasRosas': cuantasRosas,
    };
  }

  factory pRonda3.fromMap(Map<String, dynamic> map) {
    return pRonda3(
      jugador: Jugador.fromMap(map['jugador']),
      cuantasAzules: map['cuantasAzules'],
      cuantasVerdes: map['cuantasVerdes'],
      cuantasNegras: map['cuantasNegras'],
      cuantasRosas: map['cuantasRosas'],
    );
  }

  String toJson() => json.encode(toMap());

  factory pRonda3.fromJson(String source) =>
      pRonda3.fromMap(json.decode(source));
}
