import 'package:partida/paquete.dart';

void main() {
  Jugador j1 = Jugador(nombre: 'Juan');
  Jugador j2 = Jugador(nombre: 'Pancho');
  Partida p = Partida(jugadores: {j1, j2})
    ..cartasRonda1([
      pRonda1(jugador: j1, cuantasAzules: 3),
      pRonda1(jugador: j2, cuantasAzules: 1)
    ])
    ..cartasRonda2([
      pRonda2(jugador: j1, cuantasAzules: 5, cuantasVerdes: 3),
      pRonda2(jugador: j2, cuantasAzules: 6, cuantasVerdes: 2)
    ]);
  print(p);
}
