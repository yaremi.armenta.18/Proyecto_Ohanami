import 'package:partida/paquete.dart';
import 'package:test/test.dart';

void main() {
  group('Jugador', () {
    test('Debe de tener nombre no vacio', () {
      expect(() => Jugador(nombre: ''),
          throwsA(TypeMatcher<ProblemaNombreJugadorVacio>()));
    });

    test('Mismo nombre, mismo id', () {
      Jugador j1 = Jugador(nombre: 'Paco');
      Jugador j2 = Jugador(nombre: 'Paco');
      expect(j1, equals(j2));
    });

    test('Diferente nombre, Diferente id', () {
      Jugador j1 = Jugador(nombre: 'Pancho');
      Jugador j2 = Jugador(nombre: 'Paco');
      expect(j1, isNot(equals(j2)));
    });
  });
}
