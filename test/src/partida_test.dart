import 'package:partida/paquete.dart';
import 'package:partida/src/puntuacion_jugador.dart';
import 'package:test/test.dart';

void main() {
  group('Partidas', () {
    late Jugador j1, j2, j3, j4, j5, jRepetido;

    setUp(() {
      j1 = Jugador(nombre: 'Juan');
      j2 = Jugador(nombre: 'Paco');
      j3 = Jugador(nombre: 'Pepe');
      j4 = Jugador(nombre: 'Maria');
      j5 = Jugador(nombre: 'Pancho');
      jRepetido = Jugador(nombre: 'Paco');
    });

    test('Debe de tener al menos dos jugadores', () {
      expect(() => Partida(jugadores: {j1}),
          throwsA(TypeMatcher<ProblemaNumeroJugadoresMenorQueMinimo>()));
    });

    test('Debe de tener maximo 4 jugadores', () {
      expect(() => Partida(jugadores: {j1, j2, j3, j4, j5}),
          throwsA(TypeMatcher<ProblemaJugadoresMayorQueMaximo>()));
    });

    test('Con dos jugadores esta bien', () {
      expect(() => Partida(jugadores: {j1, j2}), returnsNormally);
    });
  });

  group('Puntuaciones Ronda 1', () {
    late Jugador j1, j2, j3;
    late pRonda1 p1, p2, p3;

    setUp(() {
      j1 = Jugador(nombre: 'Pancho');
      j2 = Jugador(nombre: 'Paco');
      j3 = Jugador(nombre: 'Pedro');

      p1 = pRonda1(jugador: j1, cuantasAzules: 3);
      p2 = pRonda1(jugador: j2, cuantasAzules: 6);
      p3 = pRonda1(jugador: j3, cuantasAzules: 5);
    });

    test('Jugadores diferentes no se debe', () {
      Partida p = Partida(jugadores: {j1, j2});

      expect(() => p.cartasRonda1([p1, p3]),
          throwsA(TypeMatcher<ProblemaJugadoresNoConcuerdan>()));
    });

    test('Jugadores deben concordar', () {
      Partida p = Partida(jugadores: {j1, j2});

      expect(() => p.cartasRonda1([p1, p2]), returnsNormally);
    });
  });

  group('Puntuaciones Ronda 2', () {
    late Jugador j1, j2, j3;
    late pRonda1 p11, p12, p13;
    late pRonda2 p21, p22, p23;
    late pRonda1 p11mal, p12mal;

    setUp(() {
      j1 = Jugador(nombre: 'Pancho');
      j2 = Jugador(nombre: 'Paco');
      j3 = Jugador(nombre: 'Pepe');

      p11 = pRonda1(jugador: j1, cuantasAzules: 1);
      p12 = pRonda1(jugador: j2, cuantasAzules: 1);
      p13 = pRonda1(jugador: j3, cuantasAzules: 7);

      p11mal = pRonda1(jugador: j1, cuantasAzules: 10);
      p12mal = pRonda1(jugador: j2, cuantasAzules: 10);

      p21 = pRonda2(jugador: j1, cuantasAzules: 1, cuantasVerdes: 1);
      p22 = pRonda2(jugador: j2, cuantasAzules: 2, cuantasVerdes: 2);
      p23 = pRonda2(jugador: j3, cuantasAzules: 1, cuantasVerdes: 1);
    });

    test('Jugadores diferentes manda error', () {
      Partida p = Partida(jugadores: {j1, j2});
      p.cartasRonda1([p11, p12]);
      expect(() => p.cartasRonda2([p21, p23]),
          throwsA(TypeMatcher<ProblemaJugadoresNoConcuerdan>()));
    });

    test('Debe ser llamado despues de puntuacion ronda 1', () {
      Partida p = Partida(jugadores: {j1, j2});
      expect(() => p.cartasRonda2([p21, p22]),
          throwsA(TypeMatcher<ProblemaOrdenIncorrecto>()));
    });

    test('Jugadores si concuerdan', () {
      Partida p = Partida(jugadores: {j1, j2});
      p.cartasRonda1([p11, p12]);
      expect(() => p.cartasRonda2([p21, p22]), returnsNormally);
    });

    test('Ronda 2 despues de puntuacion ronda 1', () {
      Partida p = Partida(jugadores: {j1, j2});
      p.cartasRonda1([p11, p12]);
      expect(() => p.cartasRonda2([p21, p22]), returnsNormally);
    });

    test('Cartas azules no deben ser menores al numero anterior', () {
      Partida p = Partida(jugadores: {j1, j2});
      p.cartasRonda1([p11mal, p12mal]);
      expect(() => p.cartasRonda2([p21, p22]),
          throwsA(TypeMatcher<ProblemaDisminucionAzules>()));
    });

    test('Cartas azules pueden ser iguales o mayores al numero anterior', () {
      Partida p = Partida(jugadores: {j1, j2});
      p.cartasRonda1([p11, p12]);
      expect(() => p.cartasRonda2([p21, p22]), returnsNormally);
    });

    test('Maximo de cartas azules es 20', () {
      Partida p = Partida(jugadores: {j1, j2});
      p.cartasRonda1([p11, p12]);
      expect(
          () => p.cartasRonda2([
                pRonda2(jugador: j1, cuantasAzules: 21, cuantasVerdes: 0),
                pRonda2(jugador: j2, cuantasAzules: 3, cuantasVerdes: 0)
              ]),
          throwsA(TypeMatcher<ProblemasDemasiadasAzules>()));
    });

    test('Maximo de cartas verdes es 20', () {
      Partida p = Partida(jugadores: {j1, j2});
      p.cartasRonda1([p11, p12]);
      expect(
          () => p.cartasRonda2([
                pRonda2(jugador: j1, cuantasAzules: 3, cuantasVerdes: 21),
                pRonda2(jugador: j2, cuantasAzules: 3, cuantasVerdes: 0)
              ]),
          throwsA(TypeMatcher<ProblemasDemasiadasVerdes>()));
    });

    test('Maximo de ambas es 20', () {
      Partida p = Partida(jugadores: {j1, j2});
      p.cartasRonda1([p11, p12]);
      expect(
          () => p.cartasRonda2([
                pRonda2(jugador: j1, cuantasAzules: 11, cuantasVerdes: 11),
                pRonda2(jugador: j2, cuantasAzules: 3, cuantasVerdes: 0)
              ]),
          throwsA(TypeMatcher<ProblemasDemasiadasCartas>()));
    });
  });

  group('Partida Ronda 3', () {
    late Jugador j1, j2, j3, j4;
    late pRonda1 p11, p12, p13;
    late pRonda2 p21, p22, p23;
    late pRonda3 p31, p32, p33;
    late pRonda2 p21mal, p22mal;

    setUp(() {
      j1 = Jugador(nombre: 'Pancho');
      j2 = Jugador(nombre: 'Paco');
      j3 = Jugador(nombre: 'Pepe');
      j4 = Jugador(nombre: 'Maria');

      p11 = pRonda1(jugador: j1, cuantasAzules: 1);
      p12 = pRonda1(jugador: j2, cuantasAzules: 1);
      p13 = pRonda1(jugador: j3, cuantasAzules: 1);

      p21 = pRonda2(jugador: j1, cuantasAzules: 6, cuantasVerdes: 1);
      p22 = pRonda2(jugador: j2, cuantasAzules: 2, cuantasVerdes: 2);
      p23 = pRonda2(jugador: j3, cuantasAzules: 3, cuantasVerdes: 1);

      p21mal = pRonda2(jugador: j1, cuantasAzules: 10, cuantasVerdes: 10);
      p22mal = pRonda2(jugador: j2, cuantasAzules: 2, cuantasVerdes: 10);

      p31 = pRonda3(
          jugador: j1,
          cuantasAzules: 7,
          cuantasVerdes: 7,
          cuantasNegras: 5,
          cuantasRosas: 2);
      p32 = pRonda3(
          jugador: j2,
          cuantasAzules: 3,
          cuantasVerdes: 3,
          cuantasNegras: 3,
          cuantasRosas: 3);
      p33 = pRonda3(
          jugador: j3,
          cuantasAzules: 5,
          cuantasVerdes: 5,
          cuantasNegras: 3,
          cuantasRosas: 3);
    });

    test('Debe ser llamado despues de puntuacion ronda 2', () {
      Partida p = Partida(jugadores: {j1, j2});
      expect(() => p.cartasRonda3([p31, p32]),
          throwsA(TypeMatcher<ProblemaOrdenIncorrecto>()));
    });

    test('Ronda 3 despues de ronda 2', () {
      Partida p = Partida(jugadores: {j1, j2});
      p.cartasRonda1([p11, p12]);
      p.cartasRonda2([p21, p22]);
      expect(() => p.cartasRonda3([p31, p32]), returnsNormally);
    });

    test('Jugadores diferentes manda error', () {
      Partida p = Partida(jugadores: {j1, j2});
      p.cartasRonda1([p11, p12]);
      p.cartasRonda2([p21, p22]);
      expect(() => p.cartasRonda3([p31, p33]),
          throwsA(TypeMatcher<ProblemaJugadoresNoConcuerdan>()));
    });

    test('Jugadores si concuerdan', () {
      Partida p = Partida(jugadores: {j1, j2});
      p.cartasRonda1([p11, p12]);
      p.cartasRonda2([p21, p22]);
      expect(() => p.cartasRonda3([p31, p32]), returnsNormally);
    });

    test('Cartas azules no deben ser menores al numero anterior', () {
      Partida p = Partida(jugadores: {j1, j2});
      p.cartasRonda1([p11, p12]);
      p.cartasRonda2([p21mal, p22]);
      expect(() => p.cartasRonda3([p31, p32]),
          throwsA(TypeMatcher<ProblemaDisminucionAzules>()));
    });

    test('Cartas azules pueden ser iguales o mayores al numero anterior', () {
      Partida p = Partida(jugadores: {j1, j2});
      p.cartasRonda1([p11, p12]);
      p.cartasRonda2([p21, p22]);
      expect(() => p.cartasRonda3([p31, p32]), returnsNormally);
    });

    test('Cartas verdes no deben ser menores al numero anterior', () {
      Partida p = Partida(jugadores: {j1, j2});
      p.cartasRonda1([p11, p12]);
      p.cartasRonda2([p21, p22mal]);
      expect(() => p.cartasRonda3([p31, p32]),
          throwsA(TypeMatcher<ProblemaDisminucionVerdes>()));
    });

    test('Cartas verdes pueden ser iguales o mayores al numero anterior', () {
      Partida p = Partida(jugadores: {j1, j2});
      p.cartasRonda1([p11, p12]);
      p.cartasRonda2([p21, p22]);
      expect(() => p.cartasRonda3([p31, p32]), returnsNormally);
    });

    test('Maximo de cartas azules es 30', () {
      Partida p = Partida(jugadores: {j1, j2});
      p.cartasRonda1([p11, p12]);
      p.cartasRonda2([p21, p22]);
      expect(
          () => p.cartasRonda3([
                pRonda3(
                    jugador: j1,
                    cuantasAzules: 31,
                    cuantasVerdes: 3,
                    cuantasRosas: 0,
                    cuantasNegras: 0),
                pRonda3(
                    jugador: j2,
                    cuantasAzules: 7,
                    cuantasVerdes: 3,
                    cuantasRosas: 0,
                    cuantasNegras: 0)
              ]),
          throwsA(TypeMatcher<ProblemasDemasiadasAzules>()));
    });

    test('Maximo de cartas verdes es 30', () {
      Partida p = Partida(jugadores: {j1, j2});
      p.cartasRonda1([p11, p12]);
      p.cartasRonda2([p21, p22]);
      expect(
          () => p.cartasRonda3([
                pRonda3(
                    jugador: j1,
                    cuantasAzules: 7,
                    cuantasVerdes: 31,
                    cuantasRosas: 0,
                    cuantasNegras: 0),
                pRonda3(
                    jugador: j2,
                    cuantasAzules: 7,
                    cuantasVerdes: 3,
                    cuantasRosas: 0,
                    cuantasNegras: 0)
              ]),
          throwsA(TypeMatcher<ProblemasDemasiadasVerdes>()));
    });

    test('Maximo de cartas rosas es 30', () {
      Partida p = Partida(jugadores: {j1, j2});
      p.cartasRonda1([p11, p12]);
      p.cartasRonda2([p21, p22]);
      expect(
          () => p.cartasRonda3([
                pRonda3(
                    jugador: j1,
                    cuantasAzules: 7,
                    cuantasVerdes: 3,
                    cuantasRosas: 31,
                    cuantasNegras: 0),
                pRonda3(
                    jugador: j2,
                    cuantasAzules: 7,
                    cuantasVerdes: 3,
                    cuantasRosas: 0,
                    cuantasNegras: 0)
              ]),
          throwsA(TypeMatcher<ProblemaDemasiadasRosas>()));
    });

    test('Maximo de cartas negras es 30', () {
      Partida p = Partida(jugadores: {j1, j2});
      p.cartasRonda1([p11, p12]);
      p.cartasRonda2([p21, p22]);
      expect(
          () => p.cartasRonda3([
                pRonda3(
                    jugador: j1,
                    cuantasAzules: 7,
                    cuantasVerdes: 3,
                    cuantasRosas: 0,
                    cuantasNegras: 31),
                pRonda3(
                    jugador: j2,
                    cuantasAzules: 7,
                    cuantasVerdes: 3,
                    cuantasRosas: 0,
                    cuantasNegras: 0)
              ]),
          throwsA(TypeMatcher<ProblemaDemasiadasNegras>()));
    });

    test('Maximo de todas es 30', () {
      Partida p = Partida(jugadores: {j1, j2});
      p.cartasRonda1([p11, p12]);
      p.cartasRonda2([p21, p22]);
      expect(
          () => p.cartasRonda3([
                pRonda3(
                    jugador: j1,
                    cuantasAzules: 11,
                    cuantasVerdes: 11,
                    cuantasRosas: 11,
                    cuantasNegras: 11),
                pRonda3(
                    jugador: j2,
                    cuantasAzules: 7,
                    cuantasVerdes: 3,
                    cuantasRosas: 0,
                    cuantasNegras: 0)
              ]),
          throwsA(TypeMatcher<ProblemasDemasiadasCartas>()));
    });
  });

  group('Puntuaciones', () {
    late Jugador j1, j2, j3, j4;
    late pRonda1 p11, p12;
    late pRonda2 p21, p22;
    late pRonda3 p31, p32;

    setUp(() {
      j1 = Jugador(nombre: 'Pancho');
      j2 = Jugador(nombre: 'Paco');
      j3 = Jugador(nombre: 'Pepe');
      j4 = Jugador(nombre: 'Maria');

      p11 = pRonda1(jugador: j1, cuantasAzules: 3);
      p12 = pRonda1(jugador: j2, cuantasAzules: 0);

      p21 = pRonda2(jugador: j1, cuantasAzules: 3, cuantasVerdes: 3);
      p22 = pRonda2(jugador: j2, cuantasAzules: 0, cuantasVerdes: 0);

      p31 = pRonda3(
          jugador: j1,
          cuantasAzules: 4,
          cuantasVerdes: 4,
          cuantasRosas: 1,
          cuantasNegras: 1);
      p32 = pRonda3(
          jugador: j2,
          cuantasAzules: 0,
          cuantasVerdes: 0,
          cuantasRosas: 0,
          cuantasNegras: 0);
    });

    test('Probar puntuaciones ronda 1', () {
      Partida p = Partida(jugadores: {j1, j2});
      p.cartasRonda1([p11, p12]);
      var ronda = FasePuntuacion.ronda1;
      expect(p.puntuaciones(ronda), isA<List<PuntuacionJugador>>());
    });

    test('Probar puntuaciones ronda 2', () {
      Partida p = Partida(jugadores: {j1, j2});
      p.cartasRonda1([p11, p12]);
      p.cartasRonda2([p21, p22]);
      var ronda = FasePuntuacion.ronda2;
      expect(p.puntuaciones(ronda), isA<List<PuntuacionJugador>>());
    });

    test('Probar puntuaciones ronda 3', () {
      Partida p = Partida(jugadores: {j1, j2});
      p.cartasRonda1([p11, p12]);
      p.cartasRonda2([p21, p22]);
      p.cartasRonda3([p31, p32]);
      var ronda = FasePuntuacion.ronda3;
      expect(p.puntuaciones(ronda), isA<List<PuntuacionJugador>>());
    });

    test('Probar puntuaciones ronda final', () {
      Partida p = Partida(jugadores: {j1, j2});
      p.cartasRonda1([p11, p12]);
      p.cartasRonda2([p21, p22]);
      p.cartasRonda3([p31, p32]);
      var ronda = FasePuntuacion.desenlace;
      expect(p.puntuaciones(ronda), isA<List<PuntuacionJugador>>());
    });
  });
}
