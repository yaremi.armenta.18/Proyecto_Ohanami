import 'package:partida/paquete.dart';
import 'package:test/test.dart';

void main() {
  group('Puntuacion Ronda 1', () {
    late Jugador jugador;
    setUp(() {
      jugador = Jugador(nombre: 'x');
    });

    test('La cantidad de azules debe ser cero o mayor', () {
      expect(() => pRonda1(jugador: jugador, cuantasAzules: -1),
          throwsA(TypeMatcher<ProblemasAzulesNegativas>()));
    });

    test('Maximo azules es 10', () {
      expect(() => pRonda1(jugador: jugador, cuantasAzules: 11),
          throwsA(TypeMatcher<ProblemasDemasiadasCartas>()));
    });

    test('Cantidad entre 1 y 10 esta bien', () {
      expect(
          () => pRonda1(jugador: jugador, cuantasAzules: 3), returnsNormally);
    });
  });

  group('Puntuacion Ronda 2', () {
    late Jugador jugador;
    setUp(() {
      jugador = Jugador(nombre: 'x');
    });
    test('Cartas azules no deben ser negativas', () {
      expect(
          () => pRonda2(jugador: jugador, cuantasAzules: -1, cuantasVerdes: 0),
          throwsA(TypeMatcher<ProblemasAzulesNegativas>()));
    });

    test('Se aceptan numeros positivos de cartas azules', () {
      expect(
          () => pRonda2(jugador: jugador, cuantasAzules: 3, cuantasVerdes: 3),
          returnsNormally);
    });

    test('Cartas verdes positivas', () {
      expect(
          () => pRonda2(jugador: jugador, cuantasAzules: 3, cuantasVerdes: 0),
          returnsNormally);
    });

    test('Cartas verdes no deben ser negativas', () {
      expect(
          () => pRonda2(jugador: jugador, cuantasAzules: 3, cuantasVerdes: -1),
          throwsA(TypeMatcher<ProblemaVerdesNegativas>()));
    });
  });

  group('Puntuacion Ronda 3', () {
    late Jugador jugador;
    setUp(() {
      jugador = Jugador(nombre: 'Pancho');
    });

    test('Cartas azules no deben ser negativas', () {
      expect(
          () => pRonda3(
              jugador: jugador,
              cuantasAzules: -1,
              cuantasVerdes: 0,
              cuantasRosas: 0,
              cuantasNegras: 0),
          throwsA(TypeMatcher<ProblemasAzulesNegativas>()));
    });

    test('Se aceptan numeros positivos de cartas azules', () {
      expect(
          () => pRonda3(
              jugador: jugador,
              cuantasAzules: 3,
              cuantasVerdes: 3,
              cuantasNegras: 3,
              cuantasRosas: 3),
          returnsNormally);
    });

    test('Cartas verdes positivas', () {
      expect(
          () => pRonda3(
              jugador: jugador,
              cuantasAzules: 1,
              cuantasVerdes: 2,
              cuantasNegras: 2,
              cuantasRosas: 2),
          returnsNormally);
    });

    test('Cartas verdes no deben ser negativas', () {
      expect(
          () => pRonda3(
              jugador: jugador,
              cuantasAzules: 3,
              cuantasVerdes: -1,
              cuantasNegras: 2,
              cuantasRosas: 3),
          throwsA(TypeMatcher<ProblemaVerdesNegativas>()));
    });

    test('Cartas negras deben ser positivas', () {
      expect(
          () => pRonda3(
              jugador: jugador,
              cuantasAzules: 3,
              cuantasVerdes: 2,
              cuantasNegras: 0,
              cuantasRosas: 3),
          returnsNormally);
    });

    test('Cartas negras no deben de ser negativas', () {
      expect(
          () => pRonda3(
              jugador: jugador,
              cuantasAzules: 3,
              cuantasVerdes: 2,
              cuantasNegras: -1,
              cuantasRosas: 3),
          throwsA(TypeMatcher<ProblemaNegrasNegativas>()));
    });

    test('Cartas rosas deben de ser positivas', () {
      expect(
          () => pRonda3(
              jugador: jugador,
              cuantasAzules: 3,
              cuantasVerdes: 2,
              cuantasNegras: 2,
              cuantasRosas: 0),
          returnsNormally);
    });

    test('Cartas rosas no deben se ser negativas', () {
      expect(
          () => pRonda3(
              jugador: jugador,
              cuantasAzules: 3,
              cuantasVerdes: 2,
              cuantasNegras: 2,
              cuantasRosas: -1),
          throwsA(TypeMatcher<ProblemaRosasNegativas>()));
    });
  });
}
